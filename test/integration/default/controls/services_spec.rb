# frozen_string_literal: true

control 'network-manager-service-running' do
  title 'should be running and enabled'

  describe service('NetworkManager.service') do
    it { should be_enabled }
    it { should be_running }
  end
end
