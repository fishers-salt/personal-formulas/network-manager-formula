# frozen_string_literal: true

control 'network-manager-package-install-pkg-installed' do
  title 'it should be installed'

  describe package('networkmanager') do
    it { should be_installed }
  end
end
