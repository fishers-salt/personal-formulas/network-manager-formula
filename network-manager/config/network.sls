# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as network_manager with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

network-manager-config-network-config-directory-managed:
  file.directory:
    - name: /etc/NetworkManager
    - user: root
    - group: root
    - mode: '0700'
    - require:
      - sls: {{ sls_package_install }}

network-manager-config-network-connection-directory-managed:
  file.directory:
    - name: /etc/NetworkManager/system-connections
    - user: root
    - group: root
    - mode: '0700'
    - require:
      - network-manager-config-network-config-directory-managed

{%- set wireless_networks = network_manager.get('wireless_networks', none) %}
{%- if wireless_networks is not none %}
{%- for network, data in network_manager.wireless_networks.items() %}
{%- set ssid = data.ssid %}
network-manager-config-network-wireless-{{ network }}-managed:
  file.managed:
    - name: /etc/NetworkManager/system-connections/{{ ssid }}.nmconnection
    - source: {{ files_switch(['wireless.nmconnection.tmpl'],
                lookup='network-manager-config-network-wireless-' ~ network ~ '-managed',
                )
              }}
    - mode: '0600'
    - user: root
    - group: root
    - template: jinja
    - context:
        connection: {{ data }}
    - require:
      - sls: {{ sls_package_install }}
      - network-manager-config-network-connection-directory-managed
{%- endfor %}
{%- endif %}
