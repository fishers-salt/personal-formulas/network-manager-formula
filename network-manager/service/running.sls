# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_file = tplroot ~ '.config.network' %}
{%- from tplroot ~ "/map.jinja" import mapdata as network_manager with context %}

include:
  - {{ sls_config_file }}

network-manager-service-running:
  service.running:
    - name: {{ network_manager.service.name }}
    - enable: True
