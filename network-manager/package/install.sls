# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as network_manager with context %}

network-manager-package-install-pkg-installed:
  pkg.installed:
    - name: {{ network_manager.pkg.name }}
