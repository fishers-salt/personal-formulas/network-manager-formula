# frozen_string_literal: true

control 'network-manager-config-network-config-directory-managed' do
  title 'should exist'

  describe directory('/etc/NetworkManager') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'network-manager-config-network-connection-directory-managed' do
  title 'should exist'

  describe directory('/etc/NetworkManager/system-connections') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'network-manager-config-network-wireless-test_wireless-managed' do
  title 'should match desired lines'

  describe file('/etc/NetworkManager/system-connections/test_wireless.nmconnection') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('id=test_wireless') }
    its('content') { should include('interface-name=test') }
    its('content') { should include('ssid=test_wireless') }
    its('content') { should include('psk=this is my wireless passphrase') }
  end
end
